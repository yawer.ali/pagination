import React from "react";
import {
  CardWrapper,
  Para,
  Image,
  TextWrapper,
  ImageWrapper,
} from "styles/component/UserCard/cardstyle";


function UserCard({user}) {
  console.log(user , "usersssss");
  return (
      <>
      {user.map((val)=>{
        return(
          <CardWrapper>
      <ImageWrapper>
        <Image src={val.avatar} alt="img" />
      </ImageWrapper>
      <TextWrapper>
        <Para>First Name : {val.first_name}</Para>
        <Para>Last Name : {val.last_name}</Para>
        <Para>Gender : {val.gender}</Para>
        <Para>D-O-B : {val.birthdate}</Para>
      </TextWrapper>
      </CardWrapper>
       )})}
       </>
  );
}

export default UserCard;
