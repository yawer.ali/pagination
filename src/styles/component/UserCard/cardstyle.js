import styled from "styled-components"

export const CardWrapper = styled.div`
text-align: center;
width: 20vw;
height: 50vh;
background-color: white;
box-shadow: 6px 10px 14px 26px #888888;;
`

export const TextWrapper = styled.div`
    background-color: #F5FFFA;
    width: 20vw;
    height: 25vh;
`
export const Para = styled.p`
color: Black;
font-size: 1.1vw;
font-weight: 800;

:hover{
    transform: scale(1.2,1.2);
    color: #008080;
    text-shadow: 2px 2px 5px red;
}
`
export const ImageWrapper = styled.div`
    width: 20vw;
    height: 25vh;
`
export const Image = styled.img`
    margin-top: 1.5vh;
    background-color: lightcyan;
    width: 12vw;
    height: 22vh;
    border: none;
    border-radius: 50%;
    :hover{
        transform: scale(1.1,1.1);
        background-color: #F5FFFA;
        border: 2px solid #F5FFFA;
}
`