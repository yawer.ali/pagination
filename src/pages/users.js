import React, { useState, useEffect } from "react";
import axiosInstance from "services/axiosIstance";
import UserCard from "components/usercard";
// import ReactPaginate from "react-paginate";
import Dropdownp from "components/Dropdownp";
import {
  MainWrapper,
  Heading,
  MainCardWrapper,
} from "styles/pages/users/userstyle";

function Users() {
  // const PER_PAGE = 8;
  const [currentPage, setCurrentPage] = useState(1);
  const [users, setUsers] = useState([]);
  const [userppage, setUserpPage] = useState(8);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    await axiosInstance.get("/api/users").then((res) => {
      setUsers(res?.data?.Users);
    });
  };

  // function handlePageClick({ selected: selectedPage }) {
  //   console.log("selectedPage", selectedPage);
  //   setCurrentPage(selectedPage);
  // }

  const lastuserindex = currentPage * userppage;
  const firstuserindex = lastuserindex - userppage;
  console.log("findex:", firstuserindex);
  console.log("lindex:", lastuserindex);
  const currentUsers = users.slice(firstuserindex, lastuserindex);
  // console.log("users in user", currentUsers);

  // const offset = currentPage * PER_PAGE;
  // console.log("offset", offset);

  // const pageCount = users.length / PER_PAGE;
  // console.log(pageCount);

  return (
    <MainWrapper>
      <Heading>Users</Heading>
      {/* <MainCardWrapper>
        {users.slice(offset, offset + PER_PAGE).map((user, index) => {
          return <UserCard user={user} key={user.id} />;
        })}
      </MainCardWrapper> */}

      <MainCardWrapper>
         <UserCard user={currentUsers} />
      </MainCardWrapper>

      <Dropdownp
        totalusers={users.length}
        usersppage={userppage}
        setCurrentPage={setCurrentPage}
        currentPage={currentPage}
      />

      {/* <ReactPaginate
        previousLabel={"Previous"}
        nextLabel={"Next"}
        pageCount={pageCount}
        onPageChange={handlePageClick}
        containerClassName={"pagination"}
        previousLinkClassName={"pagination__link"}
        nextLinkClassName={"pagination__link"}
        disabledClassName={"pagination__link--disabled"}
        activeClassName={"pagination__link--active"}
      /> */}
    </MainWrapper>
  );
}
export default Users;
