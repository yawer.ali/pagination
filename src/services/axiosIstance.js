import axios from 'axios'

const instance = axios.create({
  baseURL: "https://myfakeapi.com",
})

export default instance
