import styled from 'styled-components'

export const MainWrapper = styled.div`
 width: 100%;
 height: 100%;  
 .pagination{
    cursor: pointer;
    margin-top: 5vh;
    display: flex;
    justify-content: center;
    list-style: none;
}

.pagination a{
    padding: 0.7vw;
    color: white;
    font-weight: bold;
    background-color: #29ABE2;
    
}

.pagination__link--active a{
    border: 1px solid lightblue;
    border-radius: 1%;
    color: black;
    background-color: lightcyan;
}

.pagination__link--disabled a{
   color: rgb(198,197,202);
   border: 1px solid rgb(198,197,202);
}
`

export const Heading = styled.h2`
color: green;
text-shadow: 3px 3px #888888;
font-size: 2vw;
text-align: center;
`

export const MainCardWrapper = styled.div`
    width: 100vw;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    gap: 1vw;
`



