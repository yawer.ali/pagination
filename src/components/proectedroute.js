import React from "react";
import { Navigate, Outlet } from "react-router-dom";

function ProtectedRoutes() {
  const isToken = localStorage.getItem("userToken");
  return(
     isToken ? <Outlet/> : <Navigate to="/usersmanagement"/>
  )
}

export default ProtectedRoutes;  